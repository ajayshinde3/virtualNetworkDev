#!/usr/bin/env python
import os
import sys
import subprocess
import commands 
import ConfigParser

workingDir=subprocess.check_output("pwd",shell=True)
workingDir=((workingDir).splitlines())[0]

parser=ConfigParser.ConfigParser()
parser.read('hostConfig')

mac_addr = parser.get('host','mac_addr',)
ip_addr = parser.get('host','ip_addr',)
ram = parser.get('host','ram',)
imageFile = parser.get('host','imageFile',)
netmask=parser.get('host','netmask',)
switch_no = parser.get('host','switch',)
NAT = parser.get('host','NAT',)
# Allocating static ip to host and eth1 port

#mounting file system of qemu image

print "Mounting file system of "+imageFile
offsetVal=(int (commands.getoutput("fdisk -l "+imageFile+" | grep \Linux | grep \* | awk '{print $3}'")))*512
subprocess.call("mount -o loop,offset="+str(offsetVal)+" "+imageFile+" /mnt",shell=True)

#changing interface file and allocating static IP
print "Allocating static ip "+ip_addr+" in file /mnt/etc/network/interfaces of "+imageFile
os.chdir('/mnt/etc/network/')
fo1= open('interfaces.bak','a+')
content=fo1.read()
fo1.close()
fo1= open('interfaces','w+')
fo1.write(content+"\n#static ip allocation\nauto eth0\niface eth0 inet static\naddress "+ip_addr+"\nnetmask "+netmask+"\n\n\nauto eth1:0\niface eth1:0 inet static\naddress "+ip_addr+"\nnetmask "+netmask)
fo1.close()
subprocess.call("cd ",shell=True)
print "Done Allocating IP adress to machine"
subprocess.call("sync",shell=True)
subprocess.call("sleep 2s",shell=True)
os.chdir(workingDir)

#unmounting and deleting device and machine
print "Unmounting file system "
subprocess.call("umount /mnt",shell=True)
subprocess.call("cp ovs-ifup /etc/ovs-ifup_host",shell=True)
subprocess.call("cp ovs-ifdown /etc/ovs-ifdown_host",shell=True)
subprocess.call("sed -i \"s/.*switch=.*/switch=\'"+switch_no+"\'/\" /etc/ovs-ifup_host",shell=True)
subprocess.call("sed -i \"s/.*switch=.*/switch=\'"+switch_no+"\'/\" /etc/ovs-ifdown_host",shell=True)
subprocess.call("chmod 755 /etc/ovs-ifup_host",shell=True)
subprocess.call("chmod 755 /etc/ovs-ifdown_host",shell=True)
tempString = "kvm -m "+ram+" -net nic,macaddr="+mac_addr+" -net tap,script=/etc/ovs-ifup_host,downscript=/etc/ovs-ifup_host -drive file="+imageFile
subprocess.call(tempString,shell=True)
print tempString
