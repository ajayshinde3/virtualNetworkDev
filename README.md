# README #
To Create virual network using openvswitch and virtual machines to test and solve various problems from computer networking world.

### Utility used ###

* openvswitch (switches)
* qemu-kvm (guest machines)
* python 2.7x (scripting)
* linux (debian 7 or greter)

### How do I get set up? ###
* set guest system's configuration in "guestConfig"
* set host system's configuration in "hostConfig"
* set the switch's configuration in "switchConfig"
* all the script should be run using sudo
* run "ovs-kvm-install-on-host.py" on system
* run "1_createVLan --create" to create VLan as per config Files
* run "1_createVLan --delete" to delete the VLan

### Script Details ###
Still nothing added here

